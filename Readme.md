```javascript
const turbosms = require('turbosms')
// import { TurboSmsModel } from './sendSms.type'
// import TurboSms from "./sendSms.type";
// import TurboSmsInput from "./sendSms.input";

export default class SendSms {
  async auth(type:any) {
    let data = ''
    const authData = {
      'login': '', //здесь указать логин из API turboSms
      'pass': '', //здесь указать пароль из API turboSms
      'sender': '' //здесь указать название подтвержденной подписи turboSms
    }

    const found = Object.entries(authData)
    .forEach(([key, value]) => {
      if(key === type)
        data = value
    })

    return data
  }

  async balance() {
      let login = await this.auth('login')
      let pass = await this.auth('pass')

      const authRes = await turbosms.auth(login, pass)
      if(authRes === 'Неверный логин или пароль'){
        return 'Неверный логин или пароль к сервису'
      }

      const balance = await turbosms.balance()
      return balance
  }

  async sendOne(number = '', message = '', sender = '') {
      let login = await this.auth('login')
      let pass = await this.auth('pass')

      const authRes = await turbosms.auth(login, pass)
      if(authRes === 'Неверный логин или пароль'){
        return 'Неверный логин или пароль к сервису'
      }

      if( number && message ){
        if(sender == ''){
          sender = await this.auth('sender')
        }

        const result = await turbosms.sendSMS(number, message, sender)

        // if(result[0] && result[1] && number && message && sender){
        //   const turbo_sms = new TurboSmsInput
        //   turbo_sms.messageId = result[1]
        //   turbo_sms.statusMessage = result[0]
        //   turbo_sms.number = number
        //   turbo_sms.message = message
        //   turbo_sms.sender = sender
        //   turbo_sms.time = new Date().getTime()
        //   turbo_sms.status = false
        //
        //   const createDB = await TurboSmsModel.create({ ...turbo_sms })
        // }

        if(result[0] === 'Сообщения успешно отправлены'){
          // await TurboSmsModel.updateOne({ messageId: result[1] }, { status: true })
          console.log('Сообщение успешно отправлено')
          return true
        }
        return false
      }

      console.log('Ошибка в параметрах')
      return false
  }
}
```
